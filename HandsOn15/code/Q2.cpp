#include <iostream>
#include <regex>
#include <string>
using namespace std;
int main()
{
    string input;
    cin >> input;
    
    regex email_style("(.*)(@)(.*)(.)(.*)");

    bool inputIsEmail = false;
    
    inputIsEmail = regex_match(input, email_style);

    if(inputIsEmail)
        cout << "Matched" << endl;
    else
        cout << "Not Matched" << endl;
    return 0;
}