#include "Triangle.hpp"
#include <cassert>
#include <cmath>
#include <stdexcept>

class Triangle_I : public Triangle {
public:
	Triangle_I(int a, int b, int c) : Triangle(a,b,c) {}

	bool right_sides(){
		return (side1 >= side2 && side1 >= side3);
	}
};

class TriangleTest{

public:
	void run(){
		check_right_sides();
		check_constractor();
		check_get_primeter();
		check_get_area();
		check_get_kind();
	}

private:
	void check_right_sides(){
		Triangle_I t1(3,5,3);
		assert(t1.right_sides());
		Triangle_I t2(3,3,4);
		assert(t2.right_sides());
		Triangle_I t3(4,3,3);
		assert(t3.right_sides());
	}

	void check_constractor(){
		try{
		Triangle t1(1,7,5);
		Triangle t2(0,3,3);
		Triangle t3(3,3,6);
		assert(false);
		}catch(std::invalid_argument){}
	}

	void check_get_primeter(){
		Triangle t1(3,5,7);
		assert(t1.get_perimeter() == 15);
		Triangle t2(15,15,7);
		assert(t2.get_perimeter() == 37);
	}

	void check_get_area(){
		Triangle t1(3,4,5);
		assert( abs(t1.get_area() - 6) < pow(10,-6) );
		Triangle t2(5,8,5);
		assert( abs(t2.get_area() - 12) < pow(10,-6) );
		Triangle t3(2,2,2);
		assert( abs(t3.get_area() - sqrt(3)) < pow(10,-6) );
	}

	void check_get_kind(){
		Triangle t1(5,8,5);
		assert(t1.get_kind() == Triangle::Kind::ISOSCELES);
		Triangle t2(6,6,6);
		assert(t2.get_kind() == Triangle::Kind::EQUILATERAL);
		Triangle t3(3,4,5);
		assert(t3.get_kind() == Triangle::Kind::SCALENE);
	}
	
};

int main(){
	TriangleTest t;
	t.run();

	return 0;
}