#include <iostream>
#include "rsdl.demo.hpp"
#include "rsdl.hpp"
#include <cstdlib>
#include <stdlib.h>

using namespace std;

int main(){
	int window_size_x=612;
	int window_size_y=418;
	int ball_size=40;
	int moving_length=5;

	Window w(window_size_x,window_size_y);
	w.clear();

	srand(time(0));

	int ball_x=rand()%(window_size_x-ball_size);
	int ball_y=rand()%(window_size_y-ball_size);

	Event E;
	char pressedkey;
	bool again;
	int k;	
	
	while(true){
		E = w.pollForEvent();

		if(E.type()==QUIT)
			return 0;

		if(again){
			pressedkey = E.pressedKey();
			k=0;
		}
		again = true;

		switch(pressedkey){

			case 'i':
			case 'I':
				if(ball_y == 0)
					break;
				again = false;
				ball_y -= 1;					
				break;

			case 'j':
			case 'J':
				if(ball_x == 0)
					break;
				again = false;
				ball_x -= 1;
				break;

			case 'k':
			case 'K':
				if(ball_y == window_size_y-ball_size)
					break;
				again = false;
				ball_y += 1;
				break;

			case 'l':
			case 'L':
				if(ball_x == window_size_x-ball_size)
					break;
				again = false;
				ball_x += 1;
				break;



			case 'w':
			case 'W':
				if(ball_y == 0 || k++ == moving_length)
					break;
				again = false;
				ball_y -= 1;
				break;

			case 'a':
			case 'A':
				if(ball_x == 0 || k++ == moving_length)
					break;
				again = false;
				ball_x -= 1;
				break;

			case 's':
			case 'S':
				if(ball_y == window_size_y-ball_size || k++ == moving_length)
					break;
				again = false;
				ball_y += 1;
				break;


			case 'd':
			case 'D':
				if(ball_x == window_size_x-ball_size || k++ == moving_length)
					break;
				again = false;
				ball_x += 1;
				break;
		}

		w.clear();
		w.draw_bg(Football_Ground,0,0);
		w.draw_png(ball, ball_x, ball_y, ball_size, ball_size);
		w.update_screen();

		if( pressedkey=='j' || pressedkey=='l' || pressedkey=='a' || pressedkey=='d'
		 || pressedkey=='J' || pressedkey=='L' || pressedkey=='A' || pressedkey=='D')
		if( (ball_x==0 || ball_x==window_size_x-ball_size)
		 && ball_y >= 160 && ball_y <= 258-ball_size)
			{
			w.draw_bg(Goal_pic,0,0);
			w.update_screen();
			Delay(2000);
			again = true;
			
			ball_x=rand()%(window_size_x-ball_size);
			ball_y=rand()%(window_size_y-ball_size);
		}

	}
}