#include <iostream>
#include <vector>
using namespace std;


string Readnet(int lines_size){

string s;
char in;

for(int i=0; i<lines_size; i++){

cin >> in;

if( in != '-' && ( int(in) < 97 || int(in) > 122 ) )
throw 1;

else s.push_back(in);
}
if(s.length() != lines_size)
	throw 4;
return s;
}



void Read_and_Check_nets(int lines_numer, int lines_size, vector<int> &couple_lines_for_swap_0, vector<int> &couple_lines_for_swap_1){

vector<string> v;


for(int i=0; i<lines_numer; i++){


v.push_back( Readnet(lines_size) );


int rep[lines_numer]={0};
bool check_rep[lines_size] = {false};

for(int k=0; k<lines_size; k++)
for(int j=0; j<v.size()-1; j++){

if( v[j][k] == v[v.size()-1][k] && v[j][k] != '-'){
check_rep[k] = true;
rep[k]++;


}
}


for(int k=0; k<lines_size; k++){
	//cout << endl << rep[k] << endl;
if(check_rep[k]){
if(rep[k] == 0)
throw 3;
else if(rep[k] != 1)
throw 2;
}
}

}


int a[lines_numer]={0};
char aa[lines_numer];
	for(int k=0; k<lines_size; k++){
		for(int j=0; j< lines_numer; j++){
			aa[j]=v[j][k];
			
			for(int i=j-1; i>=0; i--){
				if(aa[j]==aa[i]){
					a[i]++;
					a[j]++;
				}
			}
		}
		for(int i=0; i<lines_numer; i++)
			if(a[i] != 1 && aa[i] != '-')
				throw 4;
			else
				a[i]=0;
	}


for(int k=0; k<lines_size; k++)
for(int j=0; j<v.size()-1; j++)
for(int t=j+1; t<v.size(); t++){

if( v[j][k] == v[t][k] && v[j][k] != '-'){

couple_lines_for_swap_0.push_back( j );
couple_lines_for_swap_1.push_back( t );
}
}
};


void Check_and_swap_couple(vector<int> &input, int a, int b){
int t;

if(input[a] > input[b]){
t = input[a];
input[a] = input[b];
input[b] = t;
}
}


vector<int> Read_input(int lines_numer){

vector<int> input;
int z;

for(int i=0; i<lines_numer; i++){
cin >> z;
input.push_back(z);
}

return input;
}


void Check_sorted(vector<int> input){
bool sorted=true;
for(int i=0; i<input.size()-1; i++)
	if(input[i] > input[i+1]){
		sorted = false;
		break;
}
if(sorted)
cout << "Sorted" << endl;
else
cout << "Not Sorted" << endl;
}


int main(){

int lines_numer, lines_size;
cin >> lines_numer >> lines_size;

vector<int> input;
vector<int> couple_lines_for_swap_0, couple_lines_for_swap_1;

try{

Read_and_Check_nets(lines_numer, lines_size, couple_lines_for_swap_0, couple_lines_for_swap_1);


input = Read_input(lines_numer);


for(int i=0; i<couple_lines_for_swap_0.size(); i++){

Check_and_swap_couple( input, couple_lines_for_swap_0[i], couple_lines_for_swap_1[i] );
}

Check_sorted(input);

}


catch(int a){
cout << "Invalid Network" << endl;
}

}