#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class BigNum{

	public:
		BigNum(){
			number = "0";
		}
		BigNum(string _number){
			number = _number;

			set_without_zeros();
		}

		int size(){
			return number.size();
		}


		friend BigNum operator+(BigNum first_bignum, BigNum second_bignum){
			string sum;
			
			if(first_bignum.number_of_fractions() > second_bignum.number_of_fractions())
					second_bignum.After_point_0_Adder( abs(first_bignum.number_of_fractions() - second_bignum.number_of_fractions()) );	
			else
					first_bignum.After_point_0_Adder( abs(first_bignum.number_of_fractions() - second_bignum.number_of_fractions()) );

			if(first_bignum.size() > second_bignum.size())
					second_bignum.Befor_point_0_Adder( abs(first_bignum.size() - second_bignum.size()) );	
			else
					first_bignum.Befor_point_0_Adder( abs(first_bignum.size() - second_bignum.size()) );

			string _number;
			int i = 1;
			int c = 0;
			int a = int(first_bignum.number[first_bignum.size()-i])-48;
			int b = int(second_bignum.number[second_bignum.size()-i])-48;

			while(i<=first_bignum.size()){
				i++;
				if(a<0){
					_number.push_back('.');
					a = int(first_bignum.number[first_bignum.size()-i])-48;
					b = int(second_bignum.number[second_bignum.size()-i])-48;	
					continue;
				}
				else{
					_number.push_back( char( ((a+b+c)%10)+48 ) );
					c = (a+b+c)/10;
				}
				a = int(first_bignum.number[first_bignum.size()-i])-48;
				b = int(second_bignum.number[second_bignum.size()-i])-48;
			}
				_number.push_back( c+48 );

			for(int i=0; i<_number.size(); i++)
				sum.push_back( _number[_number.size()-i-1] );

			BigNum bn(sum);
			bn.set_without_zeros();
			return bn;
		}

		BigNum& operator=(BigNum second_bignum){
			number.clear();
			this->number = second_bignum.number;
			return *this;
		}

		friend bool operator==(BigNum first_bignum, BigNum second_bignum){
			if(first_bignum.number == second_bignum.number)
				return true;
			else return false;
		}

		friend ostream& operator<<(ostream& out, const BigNum& bignum){
			out << bignum.number;
			return out;
		}

		friend istream& operator>>(istream& in, BigNum& bignum){
			string _number;
			int number_of_dot=0;
			in >> _number;

			for(int i=0; i<_number.size(); i++){
				if(int(_number[i])<46 || int(_number[i])==47 || int(_number[i])>57){
					cerr << "Wrong BigNum!" << endl;
				}
				if(_number[i] == '.') number_of_dot++;
			}
			if(number_of_dot>1){
				cerr << "Wrong BigNum!" << endl;
			}

			bignum.number = _number;
			bignum.set_without_zeros();
			return in;
		}



	private:
		string number;

		int number_of_fractions(){

			for(int i=0; i<number.size(); i++)
				if(number[i] == '.')
					return number.size()-i-1;
			return -1;
		}

		string exponent(){
			string exponent_string(number, 0, number.size()-number_of_fractions()-1);
			return exponent_string;
		}
		string fraction(){
			string fraction_string(number, number.size()-number_of_fractions(), number_of_fractions());
			return fraction_string;
		}

		void set_without_zeros(){
			int i=0, j=number.size()-1;
			bool have_dot = false;
			string _number;

			for(int s=0; s<number.size(); s++)
				if(number[s]=='.'){
					have_dot = true;
					break;
				}

			if(have_dot)
				for(; number[j]=='0'; j--);
			
			for(; number[i]=='0'; i++);

			if(!have_dot && i==number.size()){
				_number += "0";
				number = _number;
				return;
			}

			if(number[i]=='.')
				_number += "0";

			for(; i<j; i++)
				_number += number[i];
			if(number[i]!='.')
				_number += number[i];
		
			number = _number;
			return;
		}

		void After_point_0_Adder(int num_of_0){
			string zeros(num_of_0,'0');
			number += zeros;
		}

		void Befor_point_0_Adder(int num_of_0){
			string _number;
			for(int i=1; i<=number.size(); i++)
				_number.push_back( number[number.size()-i] );

			string zeros(num_of_0,'0');
			_number += zeros;

			number.clear();
			for(int i=0; i<_number.size(); i++)
				number.push_back( _number[_number.size()-i-1] );
		}
};








int main(){
	int n;
	cin >> n;
	BigNum * bignum = new BigNum[n];

	for(int s=0; s<n; s++){
		cin >> bignum[s];
	}

	int k;
	cin >> k;
	int i,j;
	string operat;

	for(int s=0; s<k; s++){
		cin >> i >> operat >> j;

		switch(operat[0]){
			case '+':
				cout << bignum[i-1] + bignum[j-1] << endl;
				break;
			case '=':
				if(bignum[i-1] == bignum[j-1])
					cout << "true" << endl;
				else
					cout << "false" << endl;
				break;
		}
	}

	return 0;	
}